<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181103233847 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entreprise DROP email, CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE nom nom VARCHAR(255) NOT NULL, CHANGE ville ville VARCHAR(255) NOT NULL, CHANGE adresse adresse VARCHAR(255) NOT NULL, CHANGE tel tel VARCHAR(255) NOT NULL, CHANGE activite activite VARCHAR(255) NOT NULL, ADD PRIMARY KEY (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entreprise MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE entreprise DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE entreprise ADD email VARCHAR(50) NOT NULL COLLATE latin1_swedish_ci, CHANGE id id INT NOT NULL, CHANGE nom nom VARCHAR(50) NOT NULL COLLATE latin1_swedish_ci, CHANGE ville ville VARCHAR(50) NOT NULL COLLATE latin1_swedish_ci, CHANGE adresse adresse VARCHAR(50) NOT NULL COLLATE latin1_swedish_ci, CHANGE tel tel VARCHAR(20) DEFAULT NULL COLLATE latin1_swedish_ci, CHANGE activite activite VARCHAR(50) NOT NULL COLLATE latin1_swedish_ci');
    }
}
