<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * @ORM\Table(name="utilisateur")
 * @UniqueEntity(fields="email")
 * @ORM\Entity()
 */
class Utilisateur implements UserInterface, \Serializable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $nom;
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $prenom;
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $classe;
    /**
     * @ORM\Column(type="integer", length=64)
     */
    private $age;
    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\Email()
     */
    private $email;
    /**
     * @Assert\Length(max=250)
     */
    private $plainPassword;
    /**
     * The below length depends on the "algorithm" you use for encoding
     * the password, but this works well with bcrypt.
     *
     * @ORM\Column(type="string", length=64)
     */
    private $password;
    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;
    /**
     * @ORM\Column(name="roles", type="array")
     */
    private $roles = array();
    public function __construct()
    {
        $this->isActive = true;
// may not be needed, see section on salt below
// $this->salt = md5(uniqid('', true));
    }
    public function getUsername()
    {
        return $this->email;
    }
    public function getSalt()
    {
// you *may* need a real salt depending on your encoder
// see section on salt below
        return null;
    }
    public function getPassword()
    {
        return $this->password;
    }
    function setPassword($password)
    {
        $this->password = $password;
    }
    public function getRoles()
    {
        if (empty($this->roles)) {
            return ['ROLE_USER'];
        }
        return $this->roles;
    }
    function addRole($role)
    {
        $this->roles[] = $role;
    }
    public function eraseCredentials()
    {
    }
    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->email,
            $this->password,
            $this->isActive,
            $this->nom,
            $this->prenom,
            $this->age,
            $this->classe,
// see section on salt below
// $this->salt,
        ));
    }
    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->email,
            $this->password,
            $this->isActive,
            $this->nom,
            $this->prenom,
            $this->age,
            $this->classe,
// see section on salt below
// $this->salt
            ) = unserialize($serialized);
    }
    function getId()
    {
        return $this->id;
    }
    function getEmail()
    {
        return $this->email;
    }
    function getPlainPassword()
    {
        return $this->plainPassword;
    }
    function getIsActive()
    {
        return $this->isActive;
    }
    function setId($id)
    {
        $this->id = $id;
    }
    function setEmail($email)
    {
        $this->email = $email;
    }
    function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }
    function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom): void
    {
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getClasse()
    {
        return $this->classe;
    }

    /**
     * @param mixed $classe
     */
    public function setClasse($classe): void
    {
        $this->classe = $classe;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param mixed $age
     */
    public function setAge($age): void
    {
        $this->age = $age;
    }

}
