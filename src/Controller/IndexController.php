<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\UtilisateurType;
use App\Entity\Utilisateur;
use App\Entity\Entreprise;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class IndexController extends AbstractController
{
    /**
     * @Route("/index", name="index")
     */
    public function index()
    {
        return $this->render('index/index.html.twig', [
            'controller_name' => 'Site EasyStage',
        ]);
    }

    /**
     * @Route("/", name="debut")
     */
    public function debut()
    {
        return $this->redirectToRoute('index');
    }

    /**
     * @Route("/inscription", name= "inscription")
     */
    public function inscriptionAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        // 1) build the form
        $user = new Utilisateur();
        $form = $this->createForm(UtilisateurType::class, $user);
        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // 3) Encode the password (you could also do this via Doctrine listener)
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            //on active par défaut
            $user->setIsActive(true);
            $user->addRole("ROLE_USER");
            // 4) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user
            $this->addFlash('success', 'Votre compte à bien été enregistré.');
            //return $this->redirectToRoute('login');
        }
        return $this->render('index/inscription.html.twig', ['form' => $form->createView(),
            'mainNavRegistration' => true, 'title' => 'Inscription']);
    }


    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils) {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        //
        $form = $this->get('form.factory')
            ->createNamedBuilder(null)
            ->add('_username', null, ['label' => 'Email'])
            ->add('_password', \Symfony\Component\Form\Extension\Core\Type\PasswordType::class,
                ['label' => 'Mot de passe'])
            ->add('ok', \Symfony\Component\Form\Extension\Core\Type\SubmitType::class, ['label' =>
                'Ok', 'attr' => ['class' => 'btn-primary btn-block']])
            ->getForm();
        return $this->render('index/login.html.twig', [
            'mainNavLogin' => true, 'title' => 'Connexion',
            //
            'form' => $form->createView(),
            'last_username' => $lastUsername,
            'error' => $error,
        ]);
    }

    /**
     * @Route("/utilisateur/supprimer/{id}", name="supprimer_utilisateur")
     */
    public function supprimerInternaute($id)
    {
        $item = $this->getDoctrine()
            ->getRepository(Utilisateur::class)
            ->find($id);
        if (!$item) {
            throw $this->createNotFoundException(
                'No product found for id ' . $id
            );
        }
     else {
            $em = $this->getDoctrine()->getManager();
            $em->remove($item);
            $em->flush();
}
        // Par défaut on retourne à la liste
        return $this->redirectToRoute('utilisateurs');

}


    /**
     * @Route("/utilisateur/modifier/{id}", name="modifier_utilisateur")
     */
    public function modifierInternaute(Request $request, $id)
    {
        $item = $this->getDoctrine()
            ->getRepository(Utilisateur::class)
            ->find($id);
        if (!$item) {
            throw $this->createNotFoundException(
                'No product found for id ' . $id
            );
        } else {
            $form = $this->createFormBuilder($item)
                ->add('nom', TextType::class)
                ->add('prenom', TextType::class)
                ->add('age', NumberType::class)
                ->add('classe', TextType::class)
                ->add('email', EmailType::class)
                ->getForm();
        }
        // Par défaut, demande POST au même contrôleur qui la restitue.
        if ($request->isMethod('POST')) {
            $form->submit($request->request->get($form->getName()));
            if ($form->isSubmitted() && $form->isValid()) {
                $item = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($item);
                $em->flush();
                return $this->redirectToRoute('utilisateurs');
            }
        }
        return $this->render('utilisateur/ajout.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/entreprise/modifier/{id}", name="modifier_entreprise")
     */
    public function modifierEntreprise(Request $request, $id)
    {
        $item = $this->getDoctrine()
            ->getRepository(Entreprise::class)
            ->find($id);
        if (!$item) {
            throw $this->createNotFoundException(
                'No product found for id ' . $id
            );
        } else {
            $form = $this->createFormBuilder($item)
                ->add('nom', TextType::class)
                ->add('ville', TextType::class)
                ->add('cp', NumberType::class)
                ->add('adresse', TextType::class)
                ->add('tel', TextType::class)
                ->add('activite', TextType::class)
                ->add('active', NumberType::class)
                ->getForm();
        }
        // Par défaut, demande POST au même contrôleur qui la restitue.
        if ($request->isMethod('POST')) {$form->submit($request->request->get($form->getName()));
            if ($form->isSubmitted() && $form->isValid()) {
                $item = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($item);
                $em->flush();
                return $this->redirectToRoute('entreprises');
            }
        }
        return $this->render('entreprise/ajout.html.twig', array(
            'form' => $form->createView(),
        ));
    }


}


