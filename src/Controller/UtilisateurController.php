<?php
namespace App\Controller;
use App\Entity\Utilisateur;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
class UtilisateurController extends Controller
{
    /**
     * @Route("/utilisateur", name="utilisateurs")
     */
    public function index()
    {
        $utilisateurs = $this->getDoctrine()
            ->getRepository(Utilisateur::class)
            ->findAll();
        return $this->render('utilisateur/index.html.twig', compact('utilisateurs'));
    }
}
