<?php
namespace App\Controller;
use App\Entity\Entreprise;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
class EntrepriseController extends Controller
{
    /**
     * @Route("/entreprise", name="entreprises")
     */
    public function index()
    {
        $entreprises = $this->getDoctrine()
            ->getRepository(Entreprise::class)
            ->findAll();
        return $this->render('entreprise/index.html.twig', compact('entreprises'));
    }
}



